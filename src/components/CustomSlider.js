import { QSlider } from 'quasar'

import { between } from 'quasar/src/utils/format.js'
import { stopAndPrevent } from 'quasar/src/utils/event.js'
import {
  getRatio,
  getModel,
  keyCodes
} from 'quasar/src/components/slider/slider-utils.js'

export default {
  name: 'CustomSlider',
  extends: QSlider,

  props: {
    preUpdateHook: Function
  },

  methods: {

    __preUpdateModel (model) {
      return this.preUpdateHook ? this.preUpdateHook(model, this.model) : model
    },

    __updateModel (model) {
      this.model = this.__preUpdateModel(model)
    },

    __updatePosition (event, dragging = this.dragging) {
      const ratio = getRatio(
        event,
        dragging,
        this.isReversed
      )

      this.__updateModel(getModel(ratio, this.min, this.max, this.step, this.decimals))
      this.curRatio = this.snap !== true || this.step === 0
        ? ratio
        : this.modelRatio
    },

    __keydown (evt) {
      if (!keyCodes.includes(evt.keyCode)) {
        return
      }

      stopAndPrevent(evt)

      const
        step = ([34, 33].includes(evt.keyCode) ? 10 : 1) * this.computedStep,
        offset = [34, 37, 40].includes(evt.keyCode) ? -step : step

      this.__updateModel(between(
        parseFloat((this.model + offset).toFixed(this.decimals)),
        this.min,
        this.max
      ))

      this.__updateValue()
    }

  }

}
