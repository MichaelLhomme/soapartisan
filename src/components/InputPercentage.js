export default {
  functional: true,

  render: function (createElement, context) {
    context.data.attrs = Object.assign({}, {
      dense: true,
      outlined: true,
      suffix: '%',
      type: 'number',
      min: '0',
      max: '100',
      style: 'width: 4em'
    }, context.data.attrs)

    return createElement('q-input', context.data)
  }
}
