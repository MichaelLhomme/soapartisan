import Oils from './oils.json'

export default {
  ...Oils,

  lauric: 'Lauric',
  myristic: 'Myristic',
  palmitic: 'Palmitic',
  stearic: 'Stearic',
  ricinoleic: 'Ricinoleic',
  oleic: 'Oleic',
  linoleic: 'Linoleic',
  linolenic: 'Linolenic',

  appName: 'Soap Creator',
  oils: 'Oils',
  receipes: 'Receipes',
  batches: 'Batches',
  settings: 'Settings',

  properties: 'Properties',
  search: 'Search',
  itemsCount: 'no items | one item | {count} items',
  none: 'None',
  name: 'Name',
  notes: 'Notes',
  edit: 'Edit',
  delete: 'Delete',
  make: 'Make'
}
