import { listTransaction, rwTransaction } from 'src/tools/DBTools.js'
import DBSchema from 'src/tools/DBSchema.js'

let DB

export default {

  async getDb () {
    return new Promise((resolve, reject) => {
      if (DB) { return resolve(DB) }

      console.log(`DB: opening ${DBSchema.DB_NAME} v${DBSchema.DB_VERSION}`)
      const request = window.indexedDB.open(DBSchema.DB_NAME, DBSchema.DB_VERSION)

      request.onerror = e => {
        console.log('DB: Error opening db', e)
        reject('Error')
      }

      request.onsuccess = e => {
        DB = e.target.result
        resolve(DB)
      }

      request.onupgradeneeded = e => {
        if (e.oldVersion < 1) {
          console.log('DB: create the initial database')
          DBSchema.createDatabase(e.target.result)
        } else {
          console.log(`DB: onupgradeneeded, version: ${e.oldVersion}`)
          DBSchema.updateDatabase(e.target.result, e.oldVersion, e.target.transaction)
        }
      }
    })
  },

  async deleteDb () {
    console.log('DB: DELETING', DB)
    const db = await this.getDb()
    db.close()
    window.indexedDB.deleteDatabase(DBSchema.DB_NAME)
    DB = null
    await this.getDb()
  },

  async importDb (jsonContent) {
    console.log('DB: Importing data')
    const db = await this.getDb()
    DBSchema.importDatabase(db, jsonContent)
  },

  async exportDb () {
    console.log('DB: Exporting data')
    const db = await this.getDb()
    return DBSchema.exportDatabase(db)
  },

  async listOils () {
    const db = await this.getDb()
    return listTransaction(db, 'oils')
  },

  async listReceipes () {
    const db = await this.getDb()
    return listTransaction(db, 'receipes')
  },

  async putReceipe (receipe) {
    const db = await this.getDb()
    return rwTransaction(db, 'receipes', store => {
      store.put(receipe)
    })
  },

  async deleteReceipe (id) {
    const db = await this.getDb()
    return rwTransaction(db, 'receipes', store => {
      store.delete(id)
    })
  },

  async listBatches () {
    const db = await this.getDb()
    return listTransaction(db, 'batches')
  },

  async putBatch (batch) {
    const db = await this.getDb()
    return rwTransaction(db, 'batches', store => {
      store.put(batch)
    })
  },

  async deleteBatch (id) {
    const db = await this.getDb()
    return rwTransaction(db, 'batches', store => {
      store.delete(id)
    })
  }

}
