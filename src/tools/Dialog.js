import DialogLayout from 'layouts/DialogLayout'

export default {
  components: { DialogLayout },

  methods: {
    async onCloseDialog () {
      return true
    },

    closeDialog () {
      this.onCloseDialog().then(res => {
        if (res) {
          this.$router.go(-1)
        }
      })
    }
  }
}
