export function fattyAcid (state) {
  return name => state.fattyAcids[name]
}

export function fattyAcids (state) {
  return Object.values(state.fattyAcids)
}

export function oils (state) {
  return Object.values(state.oils).sort((a, b) => a.name.localeCompare(b.name))
}

export function oil (state) {
  return id => state.oils[id]
}

export function receipe (state) {
  return id => state.receipes[id]
}

export function receipes (state) {
  return state.receipes
}

export function batch (state) {
  return id => state.batches[id]
}

export function batches (state) {
  return state.batches
}
