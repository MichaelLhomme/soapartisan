import Vue from 'vue'

export function discardInfoBanner (state, id) {
  Vue.set(state.infoBanners, id, false)
}

export function resetInfoBanners (state) {
  Vue.set(state, 'infoBanners', {})
}
