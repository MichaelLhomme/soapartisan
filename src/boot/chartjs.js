import Chart from 'chart.js'

import 'chartjs-plugin-colorschemes'
import 'chartjs-chart-error-bars'

import ChartDataLabels from 'chartjs-plugin-datalabels'
Chart.plugins.unregister(ChartDataLabels) // Don't use DataLabels by default
