import Vue from 'vue'

/*      */
/* Oils */
/*      */

export function updateOilsTranslations (state, translator) {
  const translatedOils = {}
  Object.values(state.oils).forEach(oil => {
    if (oil.inci) {
      translatedOils[oil.id] = { ...oil, name: translator(oil.inci) }
    } else {
      translatedOils[oil.id] = oil
    }
  })

  Vue.set(state, 'oils', translatedOils)
}

export function initOils (state, oils) {
  Vue.set(state, 'oils', oils)
}

/*          */
/* Receipes */
/*          */

export function initReceipes (state, receipes) {
  Vue.set(state, 'receipes', receipes)
}

export function addReceipe (state, receipe) {
  Vue.set(state.receipes, receipe.id, receipe)
}

export function updateReceipe (state, receipe) {
  Vue.set(state.receipes, receipe.id, receipe)
}

export function deleteReceipe (state, id) {
  Vue.delete(state.receipes, id)
}

/*         */
/* Batches */
/*         */

export function initBatches (state, batches) {
  Vue.set(state, 'batches', batches)
}

export function addBatch (state, batch) {
  Vue.set(state.batches, batch.id, batch)
}

export function updateBatch (state, batch) {
  Vue.set(state.batches, batch.id, batch)
}

export function deleteBatch (state, id) {
  Vue.delete(state.batches, id)
}
