import Oils from './oils.json'

export default {
  ...Oils,

  lauric: 'Laurique',
  myristic: 'Myristique',
  palmitic: 'Palmitique',
  stearic: 'Stéarique',
  ricinoleic: 'Ricinoléique',
  oleic: 'Oléique',
  linoleic: 'Linoléique',
  linolenic: 'Linolénique',

  appName: 'Soap Creator',
  oils: 'Huiles',
  receipes: 'Recettes',
  batches: 'Tournées',
  settings: 'Paramètres',

  properties: 'Caractéristiques',
  search: 'Rechercher',
  itemsCount: 'aucun élément | un élément | {count} éléments',
  none: 'Aucun',
  name: 'Nom',
  notes: 'Notes',
  edit: 'Editer',
  delete: 'Supprimer',
  make: 'Fabriquer'
}
