export function infoBanner (state) {
  return (id) => {
    return typeof state.infoBanners[id] === 'boolean' ? state.infoBanners[id] : true
  }
}
