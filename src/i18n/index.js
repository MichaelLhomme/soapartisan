import enUS from './en-us'
import frFR from './fr-fr'

export default {
  en: enUS,
  fr: frFR
}
