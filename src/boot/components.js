import Vue from 'vue'

// Custom components

import CustomSection from 'components/CustomSection'
Vue.component('custom-section', CustomSection)

import DescriptionItem from 'components/DescriptionItem'
Vue.component('description-item', DescriptionItem)

import Empty from 'components/Empty'
Vue.component('empty', Empty)

import InfoBanner from 'components/InfoBanner'
Vue.component('info-banner', InfoBanner)

import CustomSlider from 'components/CustomSlider'
Vue.component('custom-slider', CustomSlider)

import SimpleListItem from 'components/SimpleListItem'
Vue.component('simple-list-item', SimpleListItem)
