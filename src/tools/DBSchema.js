import { v5 as uuidv5 } from 'uuid'

import { oils, receipes } from 'src/tools/InitialData.js'
import { listTransaction, rwTransaction } from 'src/tools/DBTools.js'

export default {

  DB_NAME: 'soap_creator_db',
  DB_VERSION: 3,

  createDatabase (db) {
    db.createObjectStore('oils', { autoIncrement: true, keyPath: 'id' })
    db.createObjectStore('batches', { autoIncrement: true, keyPath: 'id' })
    const receipesStore = db.createObjectStore('receipes', { autoIncrement: true, keyPath: 'id' })

    receipesStore.transaction.oncomplete = e => {
      rwTransaction(db, 'oils', store => {
        oils.forEach(oil => {
          oil.id = uuidv5(oil.inci, uuidv5.URL)
          store.put(oil)
        })
      })
      rwTransaction(db, 'receipes', store => {
        receipes.forEach(receipe => {
          receipe.id = uuidv5(receipe.name, uuidv5.URL)
          receipe.oils = receipe.oils.map(({ oil, ratio }) => {
            return { oil: uuidv5(oil, uuidv5.URL), ratio }
          })
          store.put(receipe)
        })
      })
    }
  },

  async exportDatabase (db) {
    const oils = await listTransaction(db, 'oils')
    const receipes = await listTransaction(db, 'receipes')
    const batches = await listTransaction(db, 'batches')

    return JSON.stringify({
      oils,
      receipes,
      batches
    }, null, 2)
  },

  async importDatabase (db, jsonContent) {
    return new Promise(resolve => {
      const trans = db.transaction(['oils', 'receipes', 'batches'], 'readwrite')
      trans.oncomplete = () => {
        resolve()
      }

      if (jsonContent.oils) {
        const oilsStore = trans.objectStore('oils')
        oilsStore.clear()
        jsonContent.oils.forEach(oil => oilsStore.put(oil))
      }

      if (jsonContent.receipes) {
        const receipesStore = trans.objectStore('receipes')
        receipesStore.clear()
        jsonContent.receipes.forEach(receipe => receipesStore.put(receipe))
      }

      if (jsonContent.batches) {
        const batchesStore = trans.objectStore('batches')
        batchesStore.clear()
        jsonContent.batches.forEach(batch => batchesStore.put(batch))
      }
    })
  },

  updateDatabase (db, oldVersion, transaction) {
    if (oldVersion < 2) {
      const receipesStore = transaction.objectStore('receipes')
      receipesStore.openCursor().onsuccess = e => {
        const cursor = e.target.result
        if (cursor) {
          receipesStore.put(Object.assign(cursor.value, { additions: { oils: [], others: [] } }))
          cursor.continue()
        }
      }
    } else if (oldVersion < 3) {
      db.createObjectStore('batches', { autoIncrement: true, keyPath: 'id' })
    }
  }

}
