import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'

import base from './base'
import ui from './ui'

Vue.use(Vuex)

export default function () {
  const vuexLocal = new VuexPersistence({
    storage: window.localStorage,
    modules: ['ui']
  })

  const Store = new Vuex.Store({
    modules: {
      ui,
      base
    },
    plugins: [vuexLocal.plugin],

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  })

  return Store
}
