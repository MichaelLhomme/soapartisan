export default {
  functional: true,

  render: function (createElement, context) {
    context.data.attrs = Object.assign({}, {
      dense: true,
      outlined: true,
      type: 'number',
      suffix: 'g',
      min: '0',
      style: 'width: 5em'
    }, context.data.attrs)

    return createElement('q-input', context.data)
  }
}
