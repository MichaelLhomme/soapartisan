import { colors } from 'quasar'
const { lighten, getBrand, setBrand } = colors

const primary = getBrand('primary')
setBrand('primary-lightened', lighten(primary, 60))

const secondary = getBrand('secondary')
setBrand('secondary-lightened', lighten(secondary, 60))

const accent = getBrand('accent')
setBrand('accent-lightened', lighten(accent, 60))
